package uni.fmi.st.selenium.models;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginScreenModel {
	@FindBy(how = How.ID, using = "email")
	WebElement emailField;
	@FindBy(how = How.ID, using = "password")
	WebElement passwordField;
	@FindBy(how = How.ID, using = "submitLoginForm")
	WebElement submitButtons;
	@FindBy(how = How.ID, using = "registerButton")
	WebElement registerButtonField;
	@FindBy(how = How.ID, using = "username")
	WebElement userNameField;
	@FindBy(how = How.ID, using = "reg-email")
	WebElement regEmail;
	@FindBy(how = How.ID, using = "reg-password")
	WebElement regPasswordField;
	@FindBy(how = How.ID, using = "reg-password-2")
	WebElement regPasswordField2;
	@FindBy(how = How.ID, using = "reg-submit")
	WebElement regSubmitButton;

	public void setEmail(String email) {
		emailField.sendKeys(email);
	}

	public void setRegEmail(String email) {
		regEmail.sendKeys(email);
	}

	public String getEmail() {
		return emailField.getAttribute("value");
	}

	public void setPassword(String password) {
		passwordField.sendKeys(password);
	}

	public void setRegPassword(String password) {
		regPasswordField.sendKeys(password);
	}

	public void setRegPassword2(String password) {
		regPasswordField2.sendKeys(password);
	}

	public void setUserName(String userName) {
		userNameField.sendKeys(userName);
	}

	public String getPassword() {
		return passwordField.getAttribute("value");
	}

	public void login() {
		submitButtons.click();
	}

	public void clickRegisterButton() {
		registerButtonField.click();
	}

	public void registerFormSubmitClick() {
		regSubmitButton.click();
	}

}
