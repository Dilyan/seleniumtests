package uni.fmi.st;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author fmi
 *
 */
public class RegisterTest {
	HomeScreen homeScreen;
	RegisterForm form;

	@Before
	public void setUp() {
		homeScreen = new HomeScreen();
		form = mock(RegisterForm.class);
		System.out.println("!!!!setUp()!!!!");
	}

	@Test
	public void testRegisterWithValidEntries() {
		homeScreen.clickRegisterButton();
		assertTrue(homeScreen.isRegisterFormPresent());
		RegisterForm registerForm = homeScreen.getRegisterForm();
		registerForm.addName("Sharo");
		registerForm.addPassword("123");
		registerForm.submitData();
		assertEquals("OK", homeScreen.getRegisterMessage());
	}

	@Test
	public void testRegisterWithValidEntries4() {
		homeScreen = spy(new HomeScreen());
		homeScreen.clickRegisterButton();
		homeScreen.clickRegisterButton();
		homeScreen.clickRegisterButton();
		assertTrue(homeScreen.isRegisterFormPresent());
		RegisterForm registerForm = homeScreen.getRegisterForm();
		registerForm.addName("Sharo");
		registerForm.addPassword("123");
		registerForm.submitData();
		assertEquals("OK", homeScreen.getRegisterMessage());
		verify(homeScreen, times(3)).clickRegisterButton();
	}

	@Test
	public void testRegisterWithValidEntries2() {
		doReturn(true).when(form).isUserCreated();
		homeScreen.form = form;
		assertEquals("OK", homeScreen.getRegisterMessage());
	}

	@Test
	public void testRegisterWithValidEntries3() {
		homeScreen.form = form;
		assertEquals("NOT OK", homeScreen.getRegisterMessage());
	}
	@Test
	public void testgetPassword() {
		homeScreen.form = form;
		doReturn("123").when(form).getPassword(anyString());
		assertEquals("123", homeScreen.getPassword("Sharo"));
	}
}
