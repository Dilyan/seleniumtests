package uni.fmi.st;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
	/**
	 * test {@link App#getUserByName(String)} with null value. Expect {@code NULL}
	 * for result
	 */
	@Test
	public void testGetUserByNameWithNullValue() {
		App testInstance = new App();
		assertNull(testInstance.getUserByName(null));
	}

	/**
	 * test {@link App#getUserByName(String)} with valid value. Expect User with
	 * name "Sharo" for result
	 */
	@Test
	public void testGetUserByNameWithValidValue() {
		App testInstance = new App();
		User result = testInstance.getUserByName("Sharo");
		assertNotNull(result);
		assertEquals("Sharo", result.getName());
	}

}
