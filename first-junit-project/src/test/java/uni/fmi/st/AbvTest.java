package uni.fmi.st;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AbvTest {
	WebDriver driver;

	@BeforeClass
	public static void setUpClass() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
	}

	@Before
	public void setUp() {
		driver = new ChromeDriver();
		driver.get("http://www.abv.bg");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@Test
	public void testLoginWithWrongUser() {

	}

	@After
	public void after() throws InterruptedException {
		Thread.sleep(5000);
		driver.close();
	}
}
