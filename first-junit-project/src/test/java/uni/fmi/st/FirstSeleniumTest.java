package uni.fmi.st;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import uni.fmi.st.selenium.models.LoginScreenModel;

public class FirstSeleniumTest {

	WebDriver driver;
	private LoginScreenModel loginScreenModel;

	@BeforeClass
	public static void setUpClass() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
	}

	@Before
	public void setUp() {
		driver = new ChromeDriver();
		driver.get("http://localhost:8181/WeatherApplication-IBD-5/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		loginScreenModel = new LoginScreenModel();
		PageFactory.initElements(driver, loginScreenModel);
	}

	@Test
	public void testLogin() {
		WebElement emailField = driver.findElement(By.id("email"));
		emailField.sendKeys("sharo@abv.bg");
		assertEquals("sharo@abv.bg", emailField.getAttribute("value"));
	}

	@Test
	public void testLoginWithPageModel() {
		loginScreenModel.setEmail("sharo@abv.bg");
		assertEquals("sharo@abv.bg", loginScreenModel.getEmail());
	}

	@Test
	public void testRegistreWithPageModel() throws InterruptedException {
		Thread.sleep(2000);
		loginScreenModel.clickRegisterButton();
		Thread.sleep(2000);
		loginScreenModel.setUserName("Sharo");
		Thread.sleep(2000);
		loginScreenModel.setRegEmail("Sharo@abv.bg");
		Thread.sleep(2000);
		loginScreenModel.setRegPassword("123");
		Thread.sleep(2000);
		loginScreenModel.setRegPassword2("123");
		loginScreenModel.registerFormSubmitClick();
		assertEquals("sharo@abv.bg", loginScreenModel.getEmail());
	}

	@After
	public void after() throws InterruptedException {
		Thread.sleep(5000);
		driver.close();
	}
}
