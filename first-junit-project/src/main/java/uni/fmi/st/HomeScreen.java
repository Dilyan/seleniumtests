package uni.fmi.st;

public class HomeScreen {
	RegisterForm form;

	public void clickRegisterButton() {
		form = new RegisterForm();
		form.show();

	}

	public boolean isRegisterFormPresent() {
		return form.isPresent();
	}

	public RegisterForm getRegisterForm() {
		return form;
	}

	public String getRegisterMessage() {
		
		return form.isUserCreated()?"OK":"NOT OK";
	}
	
	public String getPassword(String name) {
		return form.getPassword(name);
	}

}
