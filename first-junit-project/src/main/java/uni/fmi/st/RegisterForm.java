package uni.fmi.st;

public class RegisterForm {

	private boolean isPresent;
	private String name;
	private String pass;
	private User user;

	public void addName(String name) {
		this.name = name;

	}

	public void addPassword(String pass) {
		this.pass = pass;

	}

	public void submitData() {
		user = new User(name, pass);

	}

	public void show() {
		isPresent = true;

	}

	public boolean isPresent() {
		return isPresent;
	}

	public boolean isUserCreated() {
		return user != null;
	}
	
	public String getPassword(String name) {
		return null;
	}

}
